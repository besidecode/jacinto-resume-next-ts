// components
import SvgIconStyle from '../../../components/SvgIconStyle';

// ----------------------------------------------------------------------

const getIcon = (name: string) => (
  <SvgIconStyle src={`/icons/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

const ICONS = {
  user: getIcon('ic_user'),
  education:getIcon('ic_banking'),
  work: getIcon('ic_kanban'),
  docs: getIcon('ic_mail'),
  projects: getIcon('ic_booking'),
  blog: getIcon('ic_blog'),
  
};

const sidebarConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'JJ Resume',
    items: [
      { title: 'Me', path: '/me', icon: ICONS.user },
      { title: 'Education', path: '/education', icon: ICONS.education },
      { title: 'Work Experience', path: '/work-experience', icon: ICONS.work },
      { title: 'Projects developed', path: '/projects-worked', icon: ICONS.projects },
      // { title: 'Blog', path: '/blog', icon: ICONS.blog },
      // { title: 'Resume docs', path: '/resume-documentation' },
    ],
  },


];

export default sidebarConfig;
