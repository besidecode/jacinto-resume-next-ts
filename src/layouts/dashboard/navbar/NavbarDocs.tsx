// @mui
import { Stack, Button, Typography } from '@mui/material';
import Hireme from 'src/components/hireme';
import { FONT_STYLE_SCRIPT } from 'src/theme/typography';
// assets
import { DocIllustration } from '../../../assets';

// ----------------------------------------------------------------------

export default function NavbarDocs() {
  return (
    <Stack
      spacing={3}
      sx={{ px: 5, pb: 5, mt: 10, width: 1, textAlign: 'center', display: 'block' }}
    >
      <DocIllustration sx={{ width: 1 }} />

      <div>
        <Typography gutterBottom variant="h3" sx={{fontFamily:FONT_STYLE_SCRIPT}}>
         Jacinto Joao
        </Typography>
        <Typography variant="body2" sx={{ color: 'text.secondary' }}>
          Looking for dev?
        </Typography>
      </div>

      <Hireme/>
    </Stack>
  );
}
