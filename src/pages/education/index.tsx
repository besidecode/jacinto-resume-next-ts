import { ReactElement } from 'react';
import { Container, Divider, Typography } from '@mui/material';
// layouts
import Layout from '../../layouts';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';

// ----------------------------------------------------------------------

Education.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

function Education() {
    const { themeStretch } = useSettings();
    return (
        <Page title="JJ Education">
        <Container maxWidth={themeStretch ? false : 'xl'}>
          <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
            School Information & Qualifications
          </Typography>
  
          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Computer Science (CS50)
          </Typography>
          <Typography gutterBottom >
            Harvard University <br/>
            Duration: 10 Months <br/>
            Concluded: 2016
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Computer Science (CSC1010H/CSC1011H)
          </Typography>
          <Typography gutterBottom >
            University of Cape Town<br/>
            start 2013 ~ dropped 2015
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Computer Science
          </Typography>
          <Typography gutterBottom >
            Polytechnic Institute and Middle Management<br/>
            Duration: 18 Months<br/>
            Concluded: 2012
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
             Grade 12 (Matric)
          </Typography>
          <Typography gutterBottom >
            Francisco Manyanga Secondary School<br/>
            Concluded: 2010
          </Typography>
          <Divider sx={{ borderStyle: 'dashed', p:1 }} />

          <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
            Online courses
          </Typography>
  
          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Introduction to Flutter development using Dart
          </Typography>
          <Typography gutterBottom >
            https://www.appbrewery.co/courses/851555/lectures/15449043 <br/>
            Instructor - Google Flutter Team 
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
           The Complete JavaScript
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/the-complete-javascript-course/learn/lecture/22628657?start=0#overview<br/>
          Instructor - Jonas Schmedtmann<br/>
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Understanding TypeScript
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/understanding-typescript/learn/lecture/17751414?start=0#overview<br/>
          Instructor - Maximilian Schwarzmüller
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Introduction to NodeJS
            </Typography>
          <Typography gutterBottom >
            https://www.appbrewery.co/courses/851555/lectures/15449043 <br/>
            Instructor - Microsoft DEV283x
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Advanced CSS and Sass
          </Typography>
          <Typography gutterBottom >
            https://www.udemy.com/course/advanced-css-and-sass/learn/lecture/8274400?start=0#overview<br/>
            Instructor -Jonas Schmedtmann
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Angular - The Complete Guide
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/the-complete-guide-to-angular-2/learn/lecture/15200410?start=0#overview<br/>
          Instructor - Maximilian Schwarzmüller
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          The Complete Web Developer Course 2.0
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/the-complete-web-developer-course-2/learn/lecture/4720366?start=0#overview<br/>
          Instructor - Rob Percival
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Introduction to ReactJS
          </Typography>
          <Typography gutterBottom >
          https://www.edx.org<br/>
          Instructor - Microsoft  DEV281x
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          The Complete React Native + Hooks
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/the-complete-react-native-and-redux-course/learn/lecture/15706390?src=sac&kw=react#overview<br/>
          Instructor - Stephen Grider
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Learn React
          </Typography>
          <Typography gutterBottom >
          https://www.youtube.com/watch?v=OH4msPNM2CI&list=PLQg6GaokU5CyvExiaMgXP_BS5WWNBfZJN<br/>
          Instructor - Anthony Sistilli
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          The Vue 3 Composition API
          </Typography>
          <Typography gutterBottom >
          https://codecourse.com/courses/the-vue-3-composition-api<br/>
          Instructor - Alex Garret
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Nuxt Authentication with Laravel Sanctum
          </Typography>
          <Typography gutterBottom >
          https://codecourse.com/courses/nuxt-authentication-with-laravel-sanctum<br/>
          Instructor - Alex Garret
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Laravel Websockets with Vue
          </Typography>
          <Typography gutterBottom >
          https://codecourse.com/courses/laravel-websockets-with-vue<br/>
          Instructor - Alex Garret
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Learn Figma - UI/UX Design Essential Training
          </Typography>
          <Typography gutterBottom >
          https://www.udemy.com/course/learn-figma/learn/lecture/8858058?start=0#overview<br/>
          Instructor - Caleb Kingston
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          AWS Developer: Building on AWS
          </Typography>
          <Typography gutterBottom >
          https://learning.edx.org/course/course-v1:AWS+OTP-AWSD1+1T2018/home<br/>
          Instructor - AWS
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          AWS Developer: Deploying on AWS
          </Typography>
          <Typography gutterBottom >
          https://learning.edx.org/course/course-v1:AWS+OTP-AWSD2+1T2018/home<br/>
          Instructor - AWS
          </Typography>
          
        </Container>
      </Page>
    )
}

export default Education;
