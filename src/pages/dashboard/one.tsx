import { ReactElement } from 'react';
import { Container, Typography } from '@mui/material';
// layouts
import Layout from '../../layouts';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';

// ----------------------------------------------------------------------

PageOne.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

// ----------------------------------------------------------------------

export default function PageOne() {
  const { themeStretch } = useSettings();

  return (
    <Page title="Jacinto Joao | Resume">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
          Hi,<br/>I'm Jacinto Joao
        </Typography>

        <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Web & Mobile developer
        </Typography>
        <Typography gutterBottom >
          Before you get busy, clicking or scrolling around my resume,<br/>
           I just wanted to take this moment to thank you in advance for taking your time to look at my resume.
        </Typography>
        <Typography gutterBottom>
         As I have mentioned above, I'm a Full-stack Web and Mobile developer, <br/>
         and I have been working as a professional software engineer for the past 7 years.
        </Typography>
        <Typography gutterBottom>
        Over the past 7 years, I've learned and built different kinds of solutions<br/> for medium and enterprise companies and/or individuals while working as an employee and freelancing.
        </Typography>

        <Typography gutterBottom>
        I'm passionate about the work that I do and probably this isn't surprising <br/>
        coming from an engineer, but I love experimenting with code, learning new languages and frameworks. <br/>
        Honestly, I enjoy the process of discovering and getting feedback or contributing to the community.
       </Typography>
      </Container>
    </Page>
  );
}
