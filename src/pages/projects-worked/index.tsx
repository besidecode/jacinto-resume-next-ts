import { ReactElement } from 'react';
import { Container, Typography, Box, Card, CardHeader, Divider } from '@mui/material';
// layouts
import Layout from '../../layouts';
import _mock, { _userCards } from '../../_mock';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';
import ProjectsCard from 'src/components/ProjectsCard';
import Image from 'src/components/Image';
import { pnQueueBusterImg, pnMentorshipAppImg } from 'src/assets/assets';

// ----------------------------------------------------------------------
const RATIO = ['4/3', '3/4', '6/4', '4/6', '16/9', '9/16', '21/9', '9/21', '1/1'] as const;
const projects = [
  {
      ration:RATIO[7],
      name:'PnP Mobile',
      cover:'https://play-lh.googleusercontent.com/85lV6e0WVXI6tOhpBkPxVIMjr4micELddE9w0H1e1nM6hgveliKq3aaW9Y8HINTVjN8=w1440-h620-rw',
      
  },
    {
      ration:RATIO[5],
      name:'iTracker Live',
      cover:'https://www.refresh.co.za/wp-content/uploads/2020/08/track_001-scaled.jpg',
      
  },
  {

    ration:RATIO[1],
    name:'Mama Money',
    cover:'https://play-lh.googleusercontent.com/xNBtBAboZ7n5liUb4InLL-j_wSXMV2a7iajDOEPbn0eWiadJZbSY1KVWrKXrBOgQcA=h750',
    
},
{
  ration:RATIO[0],
  name:'PnP Kiosk  ',
  cover:'https://jj.besidecode.com/img/kiosk01.b16d15fc.png',
  
},
{
  ration:RATIO[5],
  name:'PnP Queue Buster  ',
  cover: pnQueueBusterImg,
  
},
{
  ration:RATIO[5],
  name:'PnP Mentorship App  ',
  cover: pnMentorshipAppImg,
  
},
]

ProjectsWorked.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

// ----------------------------------------------------------------------





export default function ProjectsWorked() {
  const { themeStretch } = useSettings();



  return (
    <Page title="Projects Worked">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
         Projects Developed and Contributed
        </Typography>

        <Card>
            <Box
              sx={{
                p: 3,
                display: 'grid',
                gap: 3,
                gridTemplateColumns: {
                  xs: 'repeat(2, 1fr)',
                  sm: 'repeat(3, 1fr)',
                  md: 'repeat(3, 1fr)',
                },
              }}
            >
              {projects.map((project, index) => (
                <Box key={index+1}>
                  <Typography variant="overline" sx={{ color: 'text.secondary' }}>
                    {project.name}
                  </Typography>
                  <Image alt={project.name} src={project.cover} ratio={project.ration} sx={{ borderRadius: 1 }} />
                </Box>
              ))}
            </Box>
          </Card>

          <Divider sx={{ borderStyle: 'dashed', p:1, mt:5,mb:5 }} />

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            PnP Smart Shopper portal
          </Typography>
          <Typography gutterBottom >
            https://smartshopper.pnp.co.za
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Santam CRM
          </Typography>
          <Typography gutterBottom >
            https://www.santam.co.za/
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Expand Rand
          </Typography>
          <Typography gutterBottom >
          https://expandrand.com/
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Besidecode
          </Typography>
          <Typography gutterBottom >
            https://www.besidecode.com/
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
            Zebramail Africa
          </Typography>
          <Typography gutterBottom >
           www.zebramail.africa
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          SAMI
          </Typography>
          <Typography gutterBottom >
            www.sami.co.za
          </Typography>

          <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Fipsa
          </Typography>
          <Typography gutterBottom >
          www.fipsa.org.za
          </Typography>

      </Container>
    </Page>
  );
}
