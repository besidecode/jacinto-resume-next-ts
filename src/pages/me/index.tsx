import { ReactElement } from 'react';
import { Container, Typography } from '@mui/material';
// layouts
import Layout from '../../layouts';
// hooks
import useSettings from '../../hooks/useSettings';
import Welcome from '../../components/Welcome';
// components
import Page from '../../components/Page';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';

// ----------------------------------------------------------------------

Me.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

// ----------------------------------------------------------------------

export default function Me() {
  const { themeStretch } = useSettings();

  return (
    <Page title="Jacinto Joao | Resume">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Welcome/>
      </Container>
    </Page>
  );
}
