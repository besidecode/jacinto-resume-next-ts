import { ReactElement } from 'react';
import { Container, Typography, Grid} from '@mui/material';
// layouts
import Layout from '../../layouts';
// hooks
import useSettings from '../../hooks/useSettings';
// components
import Page from '../../components/Page';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';
import WorkExperienceCard from 'src/components/WorkExperienceCard';

// ----------------------------------------------------------------------

WorkExperience.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

// ----------------------------------------------------------------------

export default function WorkExperience() {
  const { themeStretch } = useSettings();

  return (
    <Page title="Work Experience">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
         Work Experience
        </Typography>
        <Grid container spacing={3}>

          <Grid item xs={12} sm={6} md={3}>
            <WorkExperienceCard
              companyName="Refresh Media"
              linkUrl='www.refresh.co.za'
              startEndDate='2019 ~ present'
              position='Full-Stack Web & Mobile dev'
              logoIcon={'https://www.refresh.co.za/wp-content/themes/new_refresh/img/favicon.png'}
              color="info"
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <WorkExperienceCard
               companyName="Besidecode"
               linkUrl='www.besidecode.com'
               startEndDate='2017 ~ present'
               position='Founder'
               logoIcon={'https://besidecode.com/_nuxt/img/besidecode_logo.94fe230.svg'}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <WorkExperienceCard
              companyName="MiDigitalLife"
              linkUrl='www.midigitallife.com'
              startEndDate='2016 ~ 2018'
              position='Full-Stack developer'
              color="warning"
              logoIcon={'ant-design:windows-filled'}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <WorkExperienceCard
              companyName="Design Guru"
              linkUrl='www.designguru.co.za'
              startEndDate='2014 ~ 2015'
              position='Web developer'
              color="error"
              logoIcon={'https://scontent.fcpt10-1.fna.fbcdn.net/v/t1.18169-1/p320x320/11329818_10152856940621603_4250351180589632792_n.png?_nc_cat=109&ccb=1-5&_nc_sid=1eb0c7&_nc_ohc=3E9eEPtIxBYAX-tZlkO&_nc_ht=scontent.fcpt10-1.fna&oh=00_AT8e4m6fIvVp8kfWVF17cZOXOX8SgA1oVY4xbe4urgMp2Q&oe=6203B3E6'}
            />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
