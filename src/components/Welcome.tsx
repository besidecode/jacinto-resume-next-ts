// @mui
import { styled } from '@mui/material/styles';
import { Typography, Button, Card, CardContent, CardProps, Box, Avatar } from '@mui/material';
import { SeoIllustration } from '../assets';
import { FONT_YANONE_KAFEESATZ } from 'src/theme/typography';
import Hireme from './hireme';
import { meImg } from 'src/assets/assets';

// ----------------------------------------------------------------------

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: 'none',
  textAlign: 'center',
  backgroundColor: theme.palette.secondary.lighter,
  [theme.breakpoints.up('md')]: {
    height: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
}));

// ----------------------------------------------------------------------

interface WelcomeProps extends CardProps {
  displayName?: string;
}

export default function Welcome({ displayName }: WelcomeProps) {
  return (
    <RootStyle>
      <CardContent
        sx={{
          p: { md: 0 },
          pl: { md: 5 },
          color: 'grey.800',
        }}
      >
        <Box sx={{mt:5}} />
        <Typography variant="h3" component="h1" sx={{fontFamily:FONT_YANONE_KAFEESATZ}} paragraph>
          Hi,<br/>I'm Jacinto Joao
        </Typography>

        <Typography variant="h4" sx={{fontFamily:FONT_YANONE_KAFEESATZ}}  paragraph>
          Web & Mobile developer
        </Typography>

        <Typography gutterBottom mt={3} >
        Before you get busy, clicking or scrolling around the resume,,<br/>
        I just wanted to take this moment to thank you in advance for your time looking at my resume.
        </Typography>
        <Typography gutterBottom>
        
        As I have mentioned above, I'm a Full-stack Web and Mobile developer, <br/>
        and I have been working as a professional software engineer for the past 7 years..
        </Typography>
        <Typography gutterBottom>
        Over the past 7 years, I've learned and built different types of projects<br/> 
        for medium and enterprise companies and/or individuals while working as an employee and freelancing.
        </Typography>

        <Typography gutterBottom>
        I'm intuitive in my approach, honest and practical, passionate about the work that I produce. <br/>
       </Typography>

       <Typography>
       Honestly, I enjoy the process of discovering, and making implementation across all levels of digital from the design of simple Wireframe, Sketch, PSD file or a scope of clients idea's.
       </Typography>
       <Box sx={{mt:5}} />
       <Hireme />
       <Box sx={{mt:5}} />
      </CardContent>


      <Avatar
          alt={'Jacinto Joao'}
          src={meImg}
          sx={{
           
            mx: 'auto',
            width: 360,
            height:300,
            margin: { xs: 'auto', md: 'inherit' },
            zIndex: -11,
          }}
        />

      
    </RootStyle>
  );
}
