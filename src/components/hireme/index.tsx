import { ChangeEvent, KeyboardEvent, useState } from 'react';

import {
    Box,
    Button,
    Paper,
    Stack,
    Tooltip,
    Checkbox,
    IconButton,
    OutlinedInput,
    ClickAwayListener,
    SxProps,
    Typography,
    Drawer,
  } from '@mui/material';
import MenuPopover from '../MenuPopover';

function Hireme() {

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };


    return (
        <>

        <Button variant="contained" sx={{textTransform:'none'}} onClick={handleOpen}>Hire me</Button>

        <Drawer
        open={open}
        onClose={handleOpen}
        anchor="right"
        PaperProps={{ sx: { width: { xs: 1, sm: 480, padding:20 } } }}
        >

            <Typography variant="h3" component="h1">
                Contact details
            </Typography>

            <Typography variant="subtitle1" component="p" mt={2}>
                Email: jacintotbrc@gmail.com
            </Typography>

            <Typography variant="subtitle1" component="p" mt={2}>
                Email: +27 (060) 426 3753
            </Typography>


            <Button sx={{textTransform:'none', mt:6, textAlign:'left'}} onClick={handleClose}>Close</Button>
           
        </Drawer>
        
        </>
    )
}

export default Hireme;
