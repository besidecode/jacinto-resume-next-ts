// @mui
import { alpha, styled } from '@mui/material/styles';
import { Card, Typography, Avatar, Box } from '@mui/material';
// utils
// theme
import { ColorSchema } from '../theme/palette';
// components
import Iconify from './Iconify';
import cssStyles from 'src/utils/cssStyles';

// ----------------------------------------------------------------------

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: 'none',
  textAlign: 'center',
  padding: theme.spacing(5, 0),
}));

const IconWrapperStyle = styled('div')(({ theme }) => ({
  margin: 'auto',
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  width: theme.spacing(8),
  height: theme.spacing(8),
  justifyContent: 'center',
  marginBottom: theme.spacing(3),
}));

const OverlayStyle = styled('div')(({ theme }) => ({
    ...cssStyles().bgBlur({ blur: 2, color: theme.palette.primary.darker }),
    top: 0,
    zIndex: 8,
    content: "''",
    width: '100%',
    height: '100%',
    position: 'absolute',
  }));
// ----------------------------------------------------------------------

type Props = {
  companyName: string;
  linkUrl?:string;
  position?: string;
  startEndDate?: string;
  logoIcon?: string;
  color?: ColorSchema;
};

export default function WorkExperienceCard({ companyName, linkUrl, position,startEndDate, logoIcon, color = 'primary' }: Props) {
  return (
    <RootStyle
      sx={{
        color: (theme) => theme.palette[color].darker,
        bgcolor: (theme) => theme.palette[color].lighter,
        boxShadow:(theme) => theme.customShadows.z24

      }}
    >
     <Box sx={{ position: 'relative' }}>
     <IconWrapperStyle
        sx={{
          color: (theme) => theme.palette[color].dark,
          backgroundImage: (theme) =>
            `linear-gradient(135deg, ${alpha(theme.palette[color].dark, 0)} 0%, ${alpha(
              theme.palette[color].dark,
              0.24
            )} 100%)`,
        }}
      >
        {/* <Iconify icon={logoIcon} width={24} height={24} /> */}

        <Avatar
          alt={companyName}
          src={logoIcon}
          sx={{
            width: 64,
            height: 64,
            zIndex: 11,
            left: 0,
            right: 0,
           // bottom: -32,
            mx: 'auto',
            position: 'absolute',
          }}
        />
      </IconWrapperStyle>
      <Typography variant="h3">{companyName}</Typography>

      <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
        {linkUrl}
      </Typography>

      <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
        {startEndDate}
      </Typography>

      <Typography variant="subtitle2" sx={{ opacity: 0.72 }}>
        {position}
      </Typography>
     </Box>

    </RootStyle>
  );
}
